﻿namespace SerenityTutorialMovie.Administration {
    export namespace PermissionKeys {
        export const Security = "Administration:Security";
        export const Translation = "Administration:Translation";
        export const BackgroundJob = "Administration:BackgroundJob";
        export const EmailService = "Administration:EmailService";
    }
}
