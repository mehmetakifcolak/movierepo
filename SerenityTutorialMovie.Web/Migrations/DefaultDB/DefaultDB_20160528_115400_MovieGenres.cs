﻿using FluentMigrator;
using System;

namespace SerenityTutorialMovie.Migrations.DefaultDB
{
    [Migration(20160528_115400)]
    public class DefaultDB_20160528_115400_MovieGenres : AutoReversingMigration
    {
        public override void Up()
        {
            Create.Table("MovieGenres").InSchema("mov")
                .WithColumn("MovieGenreId").AsInt32()
                    .Identity().PrimaryKey().NotNullable()
                .WithColumn("MovieId").AsInt32().NotNullable()
                    .ForeignKey("FK_MovieGenres_MovieId",
                        "mov", "Movie", "MovieId")
                .WithColumn("GenreId").AsInt32().NotNullable()
                    .ForeignKey("FK_MovieGenres_GenreId",
                        "mov", "Genre", "GenreId");
        }
    }
}