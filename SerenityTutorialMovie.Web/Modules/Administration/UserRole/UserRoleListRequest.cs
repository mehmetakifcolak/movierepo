﻿using Serenity.Services;

namespace SerenityTutorialMovie.Administration
{
    public class UserRoleListRequest : ServiceRequest
    {
        public int? UserID { get; set; }
    }
}