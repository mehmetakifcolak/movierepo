﻿using Dapper;
using Hangfire;
using Serenity.Data;
using System;
using System.Data;
using System.Data.SqlClient;

using System.Configuration;

using Serenity.Services;
using MyRow = SerenityTutorialMovie.Default.MovieRow;
using SerenityTutorialMovie.Default;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Protocols;
using Serenity;

namespace SerenityTutorialMovie.Common.Jobs
{
    public class SimpleJob
    {
        // If you want to run SQL with a connection, add this
        private readonly ISqlConnections Connections;
        public SimpleJob(ISqlConnections connections )
        {
            Connections = connections ?? throw new ArgumentNullException(nameof(connections));
        }
        
        public void Run()
        {
            string[] Title = {
                "The Matrix",
                "The Lord of the Rings: The Fellowship of the Ring",
                "The Shawshank Redemption",
                "The Godfather",
                "Pulp Fiction",
                "The Good, the Bad and the Ugly",
                "Fight Club",
            };
            string[] Description = {
               "A computer hacker learns from mysterious rebels about the true nature of his reality and his role in the war against its controllers.",
               "A meek hobbit of the Shire and eight companions set out on a journey to Mount Doom to destroy the One Ring and the dark lord Sauron.",
               "Two imprisoned men bond over a number of years, finding solace and eventual redemption through acts of common decency.",
               "The aging patriarch of an organized crime dynasty transfers control of his clandestine empire to his reluctant son.",
               "The lives of two mob hit men, a boxer, a gangster's wife, and a pair of diner bandits intertwine in four tales of violence and redemption.",
               "A bounty hunting scam joins two men in an uneasy alliance against a third in a race to find a fortune in gold buried in a remote cemetery.",
               "An insomniac office worker, looking for a way to change his life, crosses paths with a devil-may-care soap maker, forming an underground fight club that evolves into something much, much more...",
            };
            string[] Storyline = {
                "Thomas A. Anderson is a man living two lives. By day he is an average computer programmer and by night a hacker known as Neo. Neo has always questioned his reality, but the truth is far beyond his imagination. Neo finds himself targeted by the police when he is contacted by Morpheus, a legendary computer hacker branded a terrorist by the government. Morpheus awakens Neo to the real world, a ravaged wasteland where most of humanity have been captured by a race of machines that live off of the humans' body heat and electrochemical energy and who imprison their minds within an artificial reality known as the Matrix. As a rebel against the machines, Neo must return to the Matrix and confront the agents: super-powerful computer programs devoted to snuffing out Neo and the entire human rebellion.",
                "An ancient Ring thought lost for centuries has been found, and through a strange twist in fate has been given to a small Hobbit named Frodo. When Gandalf discovers the Ring is in fact the One Ring of the Dark Lord Sauron, Frodo must make an epic quest to the Cracks of Doom in order to destroy it! However he does not go alone. He is joined by Gandalf, Legolas the elf, Gimli the Dwarf, Aragorn, Boromir and his three Hobbit friends Merry, Pippin and Samwise. Through mountains, snow, darkness, forests, rivers and plains, facing evil and danger at every corner the Fellowship of the Ring must go. Their quest to destroy the One Ring is the only hope for the end of the Dark Lords reign!",
                "Andy Dufresne is a young and successful banker whose life changes drastically when he is convicted and sentenced to life imprisonment for the murder of his wife and her lover. Set in the 1940s, the film shows how Andy, with the help of his friend Red, the prison entrepreneur, turns out to be a most unconventional prisoner.",
                "When the aging head of a famous crime family decides to transfer his position to one of his subalterns, a series of unfortunate events start happening to the family, and a war begins between all the well-known families leading to insolence, deportation, murder and revenge, and ends with the favorable successor being finally chosen.",
                "Jules Winnfield and Vincent Vega are two hitmen who are out to retrieve a suitcase stolen from their employer, mob boss Marsellus Wallace. Wallace has also asked Vincent to take his wife Mia out a few days later when Wallace himself will be out of town. Butch Coolidge is an aging boxer who is paid by Wallace to lose his next fight. The lives of these seemingly unrelated people are woven together comprising of a series of funny, bizarre and uncalled-for incidents.",
                "Blondie (The Good) is a professional gunslinger who is out trying to earn a few dollars. Angel Eyes (The Bad) is a hit man who always commits to a task and sees it through, as long as he is paid to do so. And Tuco (The Ugly) is a wanted outlaw trying to take care of his own hide. Tuco and Blondie share a partnership together making money off Tuco's bounty, but when Blondie unties the partnership, Tuco tries to hunt down Blondie. When Blondie and Tuco come across a horse carriage loaded with dead bodies, they soon learn from the only survivor (Bill Carson) that he and a few other men have buried a stash of gold in a cemetery. Unfortunately Carson dies and Tuco only finds out the name of the cemetery, while Blondie finds out the name on the grave. Now the two must keep each other alive in order to find the gold. Angel Eyes (who had been looking for Bill Carson) discovers that Tuco and Blondie meet with Carson and knows they know the location of the gold. All he needs is for the two to ...",
                "A ticking-time-bomb insomniac and a slippery soap salesman channel primal male aggression into a shocking new form of therapy. Their concept catches on, with underground \"fight clubs\" forming in every town, until an eccentric gets in the way and ignites an out-of-control spiral toward oblivion.",
            };
            int[] Year = {
                1999,
                2001,
                1994,
                1972,
                1994,
                1969,
                1999
            };
            string[] ReleaseDate = {
                "1999, 03, 31",
                "2001, 12, 19",
                "1994, 10, 14",
                "1972, 03, 24",
                "1994, 10, 14",
                "1969, 01, 13",
                "1999, 10, 15"
            };
            int[] Runtime = {
                136,
                178,
                142,
                175,
                154,
                161,
                139
            };

            using (var connection = Connections.NewByKey("Default"))
            {

                using (var conn = new SqlConnection("Server=(localdb)\\MsSqlLocalDB;Database=SerenityTutorialMovie_Default_v1;Integrated Security=true"))
                {

                    conn.Open();
                    SqlDataReader rdr = null;
                    try 
                    {
                        int MovieId = 0;
                        using (var command = new SqlCommand("SELECT MAX(MovieID) FROM mov.Movie", conn))
                        { 
                            using (rdr = command.ExecuteReader())
                            {
                                while (rdr.Read())
                                {
                                    if (rdr[0].GetType().Name == "DBNull")
                                        MovieId = 0;
                                    else if ((int)rdr[0] >= Title.Length - 1)
                                        return;
                                    else
                                        MovieId = (int)rdr[0];
                                        MovieId++;
                                        Console.WriteLine(rdr[0]);
                                }
                            }
                        }
                        //string queryString = "SET IDENTITY_INSERT mov.Movie ON INSER INTO mov.Movie (Title) VALUES(@Title) SET IDENTITY_INSERT mov.Movie OFF";
                        using (var command = new SqlCommand("SET IDENTITY_INSERT mov.Movie ON INSERT INTO " +
                            "mov.Movie (MovieId, Title, Description, Storyline, Year, ReleaseDate, Runtime) " +
                            "VALUES(@MovieID, @Title, @Description, @Storyline, @Year, @ReleaseDate, @Runtime) SET IDENTITY_INSERT mov.Movie OFF", conn))
                        {
                            command.Parameters.Clear();
                            command.Parameters.AddWithValue("@MovieId", (SqlDbType)MovieId);
                            command.Parameters.AddWithValue("@Title", Title[MovieId]);
                            command.Parameters.AddWithValue("@Description", Description[MovieId]);
                            command.Parameters.AddWithValue("@Storyline", Storyline[MovieId]);
                            command.Parameters.AddWithValue("@Year", Year[MovieId]);
                            command.Parameters.AddWithValue("@ReleaseDate", DateTime.Parse(ReleaseDate[MovieId]));
                            command.Parameters.AddWithValue("@Runtime", Runtime[MovieId]);
                            command.ExecuteNonQuery();
                        }
                    conn.Close();
                    }
                    finally 
                    {
                        // close the reader
                        if (rdr != null)
                        {
                            rdr.Close();
                        }

                        // 5. Close the connection  sadık sarıgül trakya pinomatik kadir şeker
                        if (conn != null)
                        {
                            conn.Close();
                        }
                    }
                }
             }
        }
    }
}