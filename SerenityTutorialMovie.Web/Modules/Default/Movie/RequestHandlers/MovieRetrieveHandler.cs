﻿using Serenity;
using Serenity.Data;
using Serenity.Services;
using System;
using System.Data;
using MyRequest = Serenity.Services.RetrieveRequest;
using MyResponse = Serenity.Services.RetrieveResponse<SerenityTutorialMovie.Default.MovieRow>;
using MyRow = SerenityTutorialMovie.Default.MovieRow;
using Microsoft.AspNetCore.Mvc;

namespace SerenityTutorialMovie.Default
{
    public interface IMovieRetrieveHandler : IRetrieveHandler<MyRow, MyRequest, MyResponse> {}

    public class MovieRetrieveHandler : RetrieveRequestHandler<MyRow, MyRequest, MyResponse>, IMovieRetrieveHandler
    {
        public MovieRetrieveHandler(IRequestContext context)
           : base(context)
        {
        }
    }
}