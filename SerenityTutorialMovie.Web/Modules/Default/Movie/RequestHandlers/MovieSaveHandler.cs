﻿using Serenity;
using Serenity.Data;
using Serenity.Services;
using System;
using System.Data;
using MyRequest = Serenity.Services.SaveRequest<SerenityTutorialMovie.Default.MovieRow>;
using MyResponse = Serenity.Services.SaveResponse;
using MyRow = SerenityTutorialMovie.Default.MovieRow;
using System.Linq;
using Microsoft.AspNetCore.Mvc;

namespace SerenityTutorialMovie.Default
{
    public interface IMovieSaveHandler : ISaveHandler<MyRow, MyRequest, MyResponse> {}

    public class MovieSaveHandler : SaveRequestHandler<MyRow, MyRequest, MyResponse>, IMovieSaveHandler
    {
        public MovieSaveHandler(IRequestContext context)
            : base(context)
        {
        }
    }
}