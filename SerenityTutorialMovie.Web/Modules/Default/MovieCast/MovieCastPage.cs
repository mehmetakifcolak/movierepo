﻿using Serenity;
using Serenity.Web;
using Microsoft.AspNetCore.Mvc;

namespace SerenityTutorialMovie.Default.Pages
{

    [PageAuthorize(typeof(MovieCastRow))]
    public class MovieCastController : Controller
    {
        [Route("Default/MovieCast")]
        public ActionResult Index()
        {
            return View("~/Modules/Default/MovieCast/MovieCastIndex.cshtml");
        }
    }
}