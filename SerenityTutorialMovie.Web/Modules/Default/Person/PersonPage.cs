﻿using Serenity;
using Serenity.Web;
using Microsoft.AspNetCore.Mvc;

namespace SerenityTutorialMovie.Default.Pages
{

    [PageAuthorize(typeof(PersonRow))]
    public class PersonController : Controller
    {
        [Route("Default/Person")]
        public ActionResult Index()
        {
            return View("~/Modules/Default/Person/PersonIndex.cshtml");
        }
    }
}